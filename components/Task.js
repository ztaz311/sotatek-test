import React, { useRef, useState, useEffect } from 'react'
import moment from 'moment'

function Task({ status, getTaskData, task, index, data }) {
    const dueDateRef = useRef(null)
    const [dataDate, setDataDate] = useState(moment().format('YYYY-MM-DD'))
    const [state, setState] = useState({
        titleNewTask: '',
        description: '',
        dueDate: moment().format('YYYY-MM-DD'),
        priority: 'normal'
    })
    useEffect(async () => {
        if (status === 'Update') {
            await setState({ ...task })
            await setDataDate(task.dueDate)
        }
        onChangeDueDate(dueDateRef.current)
    }, [status, task])

    const onChangeDueDate = (target) => {
        setState(prevState => ({ ...prevState, dueDate: target.value || target?.attributes['data-date'].value }))
        target?.setAttribute(
            "data-date",
            moment(target?.value || target?.attributes['data-date'].value, "YYYY-MM-DD")
                .format(target.getAttribute("data-date-format"))
        )
    }

    const onChange = (e) => {
        const target = e.target
        const name = target.name
        setState({
            ...state, [name]: target.value
        })
    }


    const clearFields = () => {
        setState({
            titleNewTask: '',
            description: '',
            dueDate: moment().format('YYYY-MM-DD'),
            priority: 'normal'
        })
        dueDateRef.current.setAttribute(
            "data-date",
            moment().format(dueDateRef.current.getAttribute("data-date-format"))
        )
    }


    const onSubmit = (e) => {
        e.preventDefault()

        if (status === 'Add') {
            if (data.length > 0) {
                localStorage.setItem('taskData', JSON.stringify([...data, state]))
            } else {
                localStorage.setItem('taskData', JSON.stringify([state]))
            }
            alert('Added successful!')
        } else {
            data[index] = state
            localStorage.setItem('taskData', JSON.stringify(data))
            alert('Updated successful!')

        }
        getTaskData()
        clearFields()
    }


    return (
        <form onSubmit={onSubmit}>
            <input type="text" required value={state.titleNewTask} name="titleNewTask" onChange={onChange} className="w-full py-1 rounded border-[1px] px-2 border-gray-400 mb-5" placeholder="Add new task..." />

            <div className="font-semibold">Description</div>
            <textarea rows="5" name="description" onChange={onChange} className="w-full border-[1px] border-black pl-1" value={state.description} />

            <div className="flex md:flex-row flex-col justify-between mt-5">
                <div className="flex flex-col flex-1 mr-5 w-full">
                    <label htmlFor="dueDate" className="font-semibold">Due Date</label>
                    <input
                        id="dueDate"
                        name="dueDate"
                        type="date"
                        data-date={dataDate}
                        value={state.dueDate}
                        data-date-format="DD MMMM YYYY"
                        ref={dueDateRef}
                        onChange={(e) => onChangeDueDate(e.target)}
                        className="inputDueDate w-full h-[40px] border-[1px] px-2 border-black"
                        min={moment().format('YYYY-MM-DD')}
                    />
                </div>
                <div className="flex flex-col flex-1">
                    <label htmlFor="priority" className="font-semibold">Priority</label>
                    <select name="priority" id="priority" onChange={onChange} value={state.priority} className="w-full h-[40px] border-[1px] px-2 border-black">
                        <option value="low">Low</option>
                        <option value="normal">Normal</option>
                        <option value="high">High</option>
                    </select>
                </div>
            </div>
            <button className="w-full h-[40px] rounded mt-8 bg-[#5cb85c] text-white">{status}</button>
        </form>
    );
}

export default Task;