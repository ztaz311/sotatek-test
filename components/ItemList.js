import React, { useEffect, useState } from 'react';
import Task from '../components/Task';

function ItemList({ task, index, data, getTaskData, checkedCheckbox }) {
    const [toggle, setToggle] = useState(false)
    const [flagReload, setFlagReload] = useState(false)
    const onRemove = () => {
        if (index > -1) {
            data.splice(index, 1);
        }
        localStorage.setItem('taskData', JSON.stringify(data))
        getTaskData()
        alert('Remove successful!')
    }

    useEffect(() => {
        var timer;
        if (!toggle) {
            timer = setTimeout(() => {
                setFlagReload(toggle)
            }, 300);
        } else {
            setFlagReload(true)
        }
        return () => clearTimeout(timer)
    }, [toggle])

    return (
        <div className=" border-[1px] border-black py-1 px-5 mb-5 relative">
            <div className="py-2 flex md:flex-row flex-col  justify-between items-center">
                <div className="flex flex-row self-start items-center mr-3 " onChange={e => checkedCheckbox(e.target.checked, index)}>
                    <div className="checkbox-group">
                        <input type="checkbox" id={'task' + index} className="custom-checkbox" />
                        <span className="custom-checkbox-span"></span>
                        <label htmlFor={'task' + index} className="custom-checkbox-label cursor-pointer ml-4">{task.titleNewTask}</label>
                    </div>
                </div>
                <div className="flex-none self-end">
                    <button className="w-[100px] bg-[#01bcd4] py-1 rounded mr-2 text-white" onClick={() => setToggle(!toggle)}>{!toggle ? 'Detail' : 'Close'}</button>
                    <button className="w-[100px] bg-[#d9534f] py-1 rounded text-white" onClick={onRemove}>Remove</button>
                </div>
            </div>
            <div className="overflow-hidden h-auto" style={{ maxHeight: toggle ? 500 : 0, transition: 'max-height 0.3s ease-out' }}>
                <div className="absolute left-0 w-full border-t-[1px] mt-1 border-black"></div>
                <div className="mt-3 p-5 px-3 " >
                    {flagReload && <Task status="Update" {...{ task, index, data, getTaskData }} />}
                </div>
            </div>
        </div>
    );
}

export default React.memo(ItemList);