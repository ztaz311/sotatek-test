import Head from 'next/head'
import React, { useMemo, useEffect, useState } from 'react'
import Task from '../components/Task'
import ItemList from '../components/ItemList'
export default function Home() {
  const [data, setData] = useState([])
  const [search, setSearch] = useState('')
  var listIndexCheckBox = []

  const getTaskData = () => {
    let taskData = localStorage.getItem('taskData')
    if (taskData !== null) {
      setData(JSON.parse(taskData))
    } else {
    }
  }

  const checkedCheckbox = (valueCheckBox, index) => {
    if (valueCheckBox && listIndexCheckBox.includes(index) === false) {
      listIndexCheckBox = [...listIndexCheckBox, index]
    }

    if (!valueCheckBox && listIndexCheckBox.includes(index) === true) {
      listIndexCheckBox = listIndexCheckBox.filter(e => e !== index)
    }
  }

  const removeBulk = async () => {
    let dataTemp = [...data]
    for await (let item of listIndexCheckBox) {
      dataTemp[item] = false
    }
    var r = confirm("You are sure !");
    if (r == true) {
      let arrTemp = dataTemp.filter(s => s !== false)
      localStorage.setItem('taskData', JSON.stringify(arrTemp))
      getTaskData()
    }

  }

  const renderTasks = useMemo(() => {
    if (search !== '') {
      let resulsSearch = []
      data.filter((s, i) => {
        if (s.titleNewTask.includes(search))
          resulsSearch.push([s, i])

      })
      return resulsSearch.map((item) => {
        return (
          <ItemList key={(Math.random() + 1).toString(36).substring(7)} task={item[0]} index={item[1]} data={data} getTaskData={getTaskData} checkedCheckbox={checkedCheckbox} />
        )
      })
    }
    return data.map((item, index) => {
      return (
        <ItemList key={(Math.random() + 1).toString(36).substring(7)} task={item} index={index} data={data} getTaskData={getTaskData} checkedCheckbox={checkedCheckbox} />
      )
    })
  }, [data, search])

  useEffect(() => {
    getTaskData()
  }, [])
  useEffect(() => {

  }, [search])


  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-[#f4f4f4] px-2">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex md:flex-row flex-col md:mt-0 mt-5 main">
        <div className="md:min-w-[500px] md:h-[95vh] min-h-[500px] px-3 flex flex-col md:border-[1px] md:border-black  bg-white pt-5" style={{ borderRightColor: 'transparent' }}>
          <h1 className="font-semibold mb-4 text-center">New Task</h1>
          <Task status="Add" getTaskData={getTaskData} data={data} />
        </div>

        <div className="w-0 h-10" />

        <div className="relative flex-1">
          <div className="md:h-[95vh] h-[700px] md:min-w-[430px] min-w-[330px] flex-[2 1 auto] flex flex-col px-3 md:border-[1px] md:border-black  bg-white pt-5 overflow-scroll">
            <h1 className="font-semibold mb-4 text-center">To Do List</h1>
            <input type="text" className="w-full py-1 rounded border-[1px] px-2 border-gray-400 mb-5" placeholder="Search..." onChange={(e) => setSearch(e.target.value)} />
            <div className="mb-20">
              {renderTasks}
            </div>
          </div>
          <div className="w-full bg-[#BDBDBD] border-[1px] border-black absolute bottom-0 flex flex-row justify-between items-center px-2 py-3">
            <div className="flex-initial">Bulk Action:</div>
            <div className="flex flex-row self-end">
              <button className="w-[100px] bg-[#2296f3] py-1 rounded mr-2 text-white">Done</button>
              <button className="w-[100px] bg-[#d9534f] py-1 rounded text-white" onClick={removeBulk}>Remove</button>
            </div>
          </div>
        </div>

      </main>

      {/* <footer className="flex items-center justify-center w-full h-24 border-t">

      </footer> */}
    </div>
  )
}
